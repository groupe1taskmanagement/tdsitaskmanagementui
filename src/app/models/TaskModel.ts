export class TaskModel {
  id?: number;
  description: string;
  isCompleted?: boolean;
  dateCreation?: Date;

  constructor(id: number, description: string, etat: boolean, dateCreation: Date) {
    this.id = id;
    this.description = description;
    this.isCompleted = etat;
    this.dateCreation = dateCreation;
  }
}

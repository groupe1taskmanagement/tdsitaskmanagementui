import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment.development";
import {TaskModel} from "../models/TaskModel";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private httClient: HttpClient) { }

  public getAllTaks():Observable<TaskModel[]>{
    return this.httClient.get<TaskModel[]>(environment.apiUrl)

  }

  deleteTaskById(id: number | undefined) {
    return this.httClient
      .delete(`${environment.apiUrl}/${id}`)
  }

  createTask(task: TaskModel): Observable<TaskModel> {
    return this.httClient.post<TaskModel>(environment.apiUrl, task)
  }

  updateTask(task: TaskModel):Observable<TaskModel> {
    return this.httClient.put<TaskModel>(environment.apiUrl, task)
  }

  deleteAllTask() {
    return this.httClient.delete(`${environment.apiUrl}/deleteAll`)
  }
}

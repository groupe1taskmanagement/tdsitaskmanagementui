import {Component, OnInit} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {TaskService} from "./services/task.service";
import {TaskModel} from "./models/TaskModel";
import {JsonPipe, NgForOf, NgIf} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NgForOf, NgIf, FormsModule, JsonPipe],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {


  private _tasks: TaskModel[] = [] ;
  taskName: string = "";
  get tasks(): TaskModel[] {
    return this._tasks;
  }
  set tasks(value: TaskModel[]) {
    this._tasks = value;
  }


  constructor(private taskservice: TaskService) {

  }

  ngOnInit() {
    this.getAllTask();
  }

  addTask(){
    const task: TaskModel = {
      description: this.taskName,
      isCompleted: false
    }
    this.taskservice.createTask(task)
      .subscribe((taskCreated)=> {
        this.tasks = [taskCreated, ...this.tasks]
        alert("Task created successfully 👌")
      })
  }

  public getAllTask() {
    this.taskservice.getAllTaks()
      .subscribe(tasks => {
        this._tasks = tasks
        console.log("All task", this._tasks)
      })
  }


  deleteTask(task: TaskModel) {
    this.taskservice.deleteTaskById(task.id)
      .subscribe(()=> {
        alert("êtes vous sûr de vouloir supprimer la tâche?")
        const index = this.tasks.findIndex( item => item.id === task.id)
        if (index !== -1){
          this.tasks.splice(index,1)
        }
      })
  }

  changeTaskState(task: TaskModel) {
    console.log("Task u wann complete", task)
    task.isCompleted= !task.isCompleted
    this.taskservice.updateTask(task)
      .subscribe(task =>{
        alert(`task ${task.description} done 👍`)
      })
  }

  deleteAllTask(){
    this.tasks = []
    this.taskservice.deleteAllTask()
      .subscribe(()=>{
        alert("All task completed 🪄🪄🪄")
      })

  }

  updateTaskDescription(taskEdited: TaskModel) {
    console.log("OMG")
    console.log("task I want to etided", taskEdited)
    this.taskservice.updateTask(taskEdited)
      .subscribe(()=> {
        alert("Task updated 😉😉😉")
      })
  }
}

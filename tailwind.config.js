/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [ "./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors:{
        custom: {
          'tdsi': '#1F74B3',
          'skyblue': '#E6F4FF',
          'tdsi-700': '#0364ab',
        }
      },
    },
  },
  plugins: [],
}

